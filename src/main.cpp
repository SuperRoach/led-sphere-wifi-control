#include <ESP8266WiFi.h> //https://github.com/esp8266/Arduino
#define LED D0           // Led in NodeMCU at pin GPIO16 (D0).

//needed for library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h> //https://github.com/tzapu/WiFiManager
#include <FastLED.h>
#include <ESP_EEPROM.h>

// Set web server port number to 80
WiFiServer server(80);

// Variable to store the HTTP request
String header;

// Auxiliar variables to store the current output state
String onboard_led_state = "off";
String output4State = "off";
uint running_pattern = 3; // This is overwritten by the eeprom inital value in setup

// Setup the saved user parameters from eeprom.
struct MyEEPROMStruct
{
  int anInteger;
  float aFloating;
  int anotherInteger;
  byte someBytes[12];
  boolean state;
} eeprom_brightness, eeprom_led_pattern;

// if we have a notification, set it's details here.
bool notification = false;
bool notification_color_reached = false;

// I'm having trouble with some patterns patterns overriding the notification
// and not incrementing the timer, giving me some trouble with them staying on or not looking visible.
// This sets the pattern to nothing while a notifcation is on.
// named after an awesome boss, best of luck over there!
bool asherhack = true;

CRGB notification_color = CRGB::Red;
// in msecs
unsigned long notification_hold_time = 10000;
// Reset the hold time
unsigned long current_hold_time = 0;
CRGB sanity_notification_previous_pixel = CRGB::Black;

uint8_t hue = 0; // rotating "base color" used by many of the patterns

// Assign output variables to GPIO pins
const int output5 = 5;
const int output4 = 4;

// FastLED stuff
#define NUM_LEDS 90
#define DATA_PIN D8
#define BRIGHTNESS 164
#define NOTIFICATION_BRIGHTNESS 255
#define FRAMES_PER_SECOND 90

int delay_amount = 1000 / FRAMES_PER_SECOND;
// Padding delay to stop the notifications disapearing too fast. Yay science.
int buffer_amount = 8;
// Define the array of leds
CRGB leds[NUM_LEDS];

// Helper functions mostly for FastLED
// Fading FX tips from https://gist.github.com/kriegsman/d0a5ed3c8f38c64adcb4837dafb6e690
// Helper function that blends one uint8_t toward another by a given amount
void nblend_U8_towards_U8(uint8_t &cur, const uint8_t target, uint8_t amount)
{
  if (cur == target)
  {
    return;
  }

  // if (cur == target)
  // {
  //   // We run at a fixed frame rate and need to increment by that value to accurately follow the ms delay given.

  //   current_hold_time++;
  //   // = current_hold_time + delay_amount;
  //   if (current_hold_time >= notification_hold_time)
  //   {
  //     Serial.print("time to stop");
  //     // Stop displaying this effect
  //     notification = false;
  //   }
  //   return;
  // }

  if (cur < target)
  {
    uint8_t delta = target - cur;
    delta = scale8_video(delta, amount);
    cur += delta;
  }
  else
  {
    uint8_t delta = cur - target;
    delta = scale8_video(delta, amount);
    cur -= delta;
  }
}

// Blend one CRGB color toward another CRGB color by a given amount.
// Blending is linear, and done in the RGB color space.
// This function modifies 'cur' in place.
CRGB fade_towards_color(CRGB &cur, const CRGB &target, uint8_t amount)
{
  nblend_U8_towards_U8(cur.red, target.red, amount);
  nblend_U8_towards_U8(cur.green, target.green, amount);
  nblend_U8_towards_U8(cur.blue, target.blue, amount);
  return cur;
}

// Increment a timer when the colour is being held.
void fade_towards_notification_color(CRGB *L, uint16_t N, const CRGB &bgColor, uint8_t fadeAmount)
{
  CRGB led_pixel_colour = L[1];

  for (uint16_t i = 0; i < N; i++)
  {
    fade_towards_color(L[i], bgColor, fadeAmount);
  }

  Serial.print(". ");
  Serial.print(L[20].r);
  Serial.print("|");
  Serial.print(L[20].g);
  Serial.print("|");
  Serial.print(L[20].b);
  Serial.print(" / ");
  Serial.print(bgColor.r);
  Serial.print("|");
  Serial.print(bgColor.g);
  Serial.print("|");
  Serial.print(bgColor.b);
  Serial.print(" ");
  Serial.println("--");



  if (sanity_notification_previous_pixel == L[1])
  {
    Serial.print(" pixel colour hasn't changed, that's good ");
  }
  
  // if (L[1].g == bgColor.g )
  if (led_pixel_colour == bgColor )
  {
    // We buffer out variations and layered effects from some patterns by jamming it on if it ever reaches the colour.
    notification_color_reached = true;
  }
  // We've starting showing all the colours as the notification colour, start incrementing the hold time.
  // We run at a fixed frame rate and need to increment by that value to accurately follow the ms delay given.

  // The delay time does not seem to be accurate, so the last int is a fudge factor to make it seem closer.
  if (notification_color_reached)
  {
    current_hold_time = current_hold_time + delay_amount + buffer_amount;
    Serial.print(current_hold_time);
    Serial.print("ms ");
    if (current_hold_time >= notification_hold_time)
    {
      Serial.print("time to stop notification. ");
      // Stop displaying this effect
      current_hold_time = 0;
      notification = false;
      notification_color_reached = false;
    }
  }
  sanity_notification_previous_pixel = L[1];
}

// Fade an entire array of CRGBs toward a given background color by a given amount
// This function modifies the pixel array in place.
void fade_towards_color(CRGB *L, uint16_t N, const CRGB &bgColor, uint8_t fadeAmount)
{
  for (uint16_t i = 0; i < N; i++)
  {
    fade_towards_color(L[i], bgColor, fadeAmount);
  }
}

void notifications()
{
  if (notification)
  {
    LEDS.setBrightness(NOTIFICATION_BRIGHTNESS);
    // Fade towards the specified colour.
    fade_towards_notification_color(leds, NUM_LEDS, notification_color, 10);
    // fade_towards_color(leds, NUM_LEDS, notification_color, 10);
  }
  else
  {
    LEDS.setBrightness(BRIGHTNESS);
  }
}

//
// FastLED Loops and procedural animations that are called.

void colors_fadeall()
{
  for (int i = 0; i < NUM_LEDS; i++)
  {
    leds[i].nscale8(250);
  }
}

void cylon()
{
  // TODO: This iterates a full cycle every loop - it needs to have a incremental phase like juggle to make it work with notifications
  static uint8_t hue = 0;
  // Serial.print("x");
  // First slide the led in one direction
  for (int i = 0; i < NUM_LEDS; i++)
  {
    // Set the i'th led to red
    leds[i] = CHSV(hue++, 255, 255);
    // Show the leds
    FastLED.show();
    // now that we've shown the leds, reset the i'th led to black
    // leds[i] = CRGB::Black;
    colors_fadeall();
    // Wait a little bit before we loop around and do it again
    FastLED.delay(10);
  }
  Serial.print("x");

  // Now go in the other direction.
  for (int i = (NUM_LEDS)-1; i >= 0; i--)
  {
    // Set the i'th led to red
    leds[i] = CHSV(hue++, 255, 255);
    // Show the leds
    FastLED.show();
    // now that we've shown the leds, reset the i'th led to black
    // leds[i] = CRGB::Black;
    colors_fadeall();
    // Wait a little bit before we loop around and do it again
    delay(10);
  }
}

void rainbow()
{
  // FastLED's built-in rainbow generator
  fill_rainbow(leds, NUM_LEDS, hue, 7);
  notifications();
}

void add_glitter(fract8 chanceOfGlitter)
{
  if (random8() < chanceOfGlitter)
  {
    leds[random16(NUM_LEDS)] += CRGB::White;
  }
}
void rainbow_with_glitter()
{
  // built-in FastLED rainbow, plus some random sparkly glitter
  rainbow();
  notifications();
  add_glitter(80);
}

void confetti()
{
  // random colored speckles that blink in and fade smoothly
  fadeToBlackBy(leds, NUM_LEDS, 10);
  int pos = random16(NUM_LEDS);
  leds[pos] += CHSV(hue + random8(64), 200, 255);
}

void sinelon()
{
  // a colored dot sweeping back and forth, with fading trails
  fadeToBlackBy(leds, NUM_LEDS, 20);
  int pos = beatsin16(13, 0, NUM_LEDS - 1);
  leds[pos] += CHSV(hue, 255, 192);
}

void bpm()
{
  // colored stripes pulsing at a defined Beats-Per-Minute (BPM)
  uint8_t BeatsPerMinute = 62;
  CRGBPalette16 palette = PartyColors_p;
  uint8_t beat = beatsin8(BeatsPerMinute, 64, 255);
  for (int i = 0; i < NUM_LEDS; i++)
  { //9948
    leds[i] = ColorFromPalette(palette, hue + (i * 2), beat - hue + (i * 10));
  }
}

void juggle()
{
  // eight colored dots, weaving in and out of sync with each other
  fadeToBlackBy(leds, NUM_LEDS, 20);
  byte dothue = 0;
  for (int i = 0; i < 8; i++)
  {
    leds[beatsin16(i + 7, 0, NUM_LEDS - 1)] |= CHSV(dothue, 200, 255);
    dothue += 32;
  }
}

// Breathing effect
// sourced from https://github.com/marmilicious/FastLED_examples/blob/master/breath_effect_v2.ino
static float pulseSpeed = 0.5; // Larger value gives faster pulse.

uint8_t hueA = 66;      // Start hue at valueMin.
uint8_t satA = 200;     // Start saturation at valueMin.
float valueMin = 120.0; // Pulse minimum value (Should be less then valueMax).

uint8_t hueB = 83;      // End hue at valueMax.
uint8_t satB = 255;     // End saturation at valueMax.
float valueMax = 200.0; // Pulse maximum value (Should be larger then valueMin).

uint8_t breathingHue = hueA;                             // Do Not Edit
uint8_t breathingSat = satA;                             // Do Not Edit
float val = valueMin;                                    // Do Not Edit
uint8_t hueDelta = hueA - hueB;                          // Do Not Edit
static float delta = (valueMax - valueMin) / 2.35040238; // Do Not Edit

void breathing()
{
  // Gives a breathing effect between two colours.
  float dV = ((exp(sin(pulseSpeed * millis() / 2000.0 * PI)) - 0.36787944) * delta);
  val = valueMin + dV;
  breathingHue = map(val, valueMin, valueMax, hueA, hueB); // Map hue based on current val
  breathingSat = map(val, valueMin, valueMax, satA, satB); // Map sat based on current val

  for (int i = 0; i < NUM_LEDS; i++)
  {
    leds[i] = CHSV(breathingHue, breathingSat, val);
  }
}

void nothing()
{
  // Don't run an animation, just fade to black.
  colors_fadeall();
  FastLED.delay(15);
}

void static_blue()
{
  // Not an animation, a colour
  // fade_towards_color(leds, NUM_LEDS, CRGB::Blue, 3);
  fill_solid(leds, NUM_LEDS, CRGB::Blue);
  // notifications();
}

void static_sun()
{
  // Not an animation, a colour
  // fade_towards_color(leds, NUM_LEDS, CRGB::Gold, 3);
  fill_solid(leds, NUM_LEDS, CRGB::Gold);
}

void static_cool_white()
{
  // Not an animation, a colour
  // fade_towards_color(leds, NUM_LEDS, CRGB::Gold, 3);
  fill_solid(leds, NUM_LEDS, CRGB::FloralWhite);
}

void setup()
{
  delay(3000);
  Serial.begin(115200);

  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
  WiFiManagerParameter custom_text("<p>This is the Ping pong sphere of <del>death</del> awesomeness</p>");
  wifiManager.addParameter(&custom_text);

  // Wait this many seconds before giving up and running a default loop.
  wifiManager.setConfigPortalTimeout(180);

  eeprom_brightness.anInteger = BRIGHTNESS;
  eeprom_led_pattern.anInteger = 3;
  EEPROM.begin(sizeof(MyEEPROMStruct));

  // If we have previously saved user data, restore it first.
  if (EEPROM.percentUsed() >= 0)
  {
    EEPROM.get(0, running_pattern);
    Serial.println("Restored users saved settings.");
    // Brightness not used yet.
  }

  //reset saved settings
  // wifiManager.resetSettings();

  //set custom ip for portal
  //wifiManager.setAPStaticIPConfig(IPAddress(10,0,1,1), IPAddress(10,0,1,1), IPAddress(255,255,255,0));

  //fetches ssid and pass from eeprom and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  wifiManager.autoConnect("DeathSphereAP");
  // wifiManager.autoConnect("AutoConnectAP", "localchicken")

  //or use this for auto generated name ESP + ChipID
  //wifiManager.autoConnect();

  //if you get here you have connected to the WiFi
  Serial.println("connected...yey happy dance :)");
  server.begin();
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(300);
  digitalWrite(LED_BUILTIN, LOW);
  delay(300);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(300);
  digitalWrite(LED_BUILTIN, LOW);
  delay(300);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(300);
  digitalWrite(LED_BUILTIN, LOW);

  FastLED.addLeds<WS2811, DATA_PIN, RGB>(leds, NUM_LEDS);
  LEDS.setBrightness(BRIGHTNESS);
}

// List of patterns to cycle through.  Each is defined as a separate function.
typedef void (*simple_pattern_list[])();
simple_pattern_list patterns = {cylon, rainbow, rainbow_with_glitter, sinelon, breathing, confetti, juggle, bpm, static_blue, static_sun, static_cool_white, nothing};
// ... As well as in the same order, Their friendly name for the front end.
char const *pattern_names_string[] = {"Cylon", "Rainbow", "Rainbow with glitter",
                                      "Sinelon", "Breathing", "Confetti", "Juggle", "Bpm", "Static blue", "Static sun", "Static cool white", "Nothing"};

void loop()
{
  // put your main code here, to run repeatedly:
  WiFiClient client = server.available(); // Listen for incoming clients

  if (client) // If a new client connects,
  {
    Serial.println("New Client connected, quick make the place look clean... "); // print a message out in the serial port
    String currentLine = "";                                                     // make a String to hold incoming data from the client
    while (client.connected())
    { // loop while the client's connected
      if (client.available())
      {                         // if there's bytes to read from the client,
        char c = client.read(); // read a byte, then
        header += c;
        if (c == '\n')
        { // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0)
          {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();

            // turns the GPIOs on and off
            if (header.indexOf("GET /led/on") >= 0)
            {
              onboard_led_state = "on";
              digitalWrite(LED_BUILTIN, HIGH);
            }
            else if (header.indexOf("GET /led/off") >= 0)
            {
              onboard_led_state = "off";
              digitalWrite(LED_BUILTIN, LOW);
            }
            else if (header.indexOf("GET /led/on") >= 0)
            {
              Serial.println("GPIO 4 on");
              output4State = "on";
              digitalWrite(output4, HIGH);
            }
            else if (header.indexOf("GET /4/off") >= 0)
            {
              Serial.println("GPIO 4 off");
              output4State = "off";
              digitalWrite(output4, LOW);
            }
            // Funny story: So the order for the pattern numbers here are in reverse, because the indexOf
            // pattern matching was picking up the wrong patterns once we got to above 10.
            else if (header.indexOf("GET /pattern/black") >= 0 || header.indexOf("GET /pattern/11") >= 0)
            {
              Serial.println("Pattern for no animation/black");
              running_pattern = 11;
            }
            else if (header.indexOf("GET /pattern/staticcoolwhite") >= 0 || header.indexOf("GET /pattern/10") >= 0)
            {
              Serial.println("Pattern for Static Cool white");
              running_pattern = 10;
            }
            else if (header.indexOf("GET /pattern/staticsun") >= 0 || header.indexOf("GET /pattern/9") >= 0)
            {
              Serial.println("Pattern for Static Sun");
              running_pattern = 9;
            }
            else if (header.indexOf("GET /pattern/staticblue") >= 0 || header.indexOf("GET /pattern/8") >= 0)
            {
              Serial.println("Pattern for Static blue");
              running_pattern = 8;
            }
            else if (header.indexOf("GET /pattern/bpm") >= 0 || header.indexOf("GET /pattern/7") >= 0)
            {
              Serial.println("Pattern for BPM");
              running_pattern = 7;
            }
            else if (header.indexOf("GET /pattern/juggle") >= 0 || header.indexOf("GET /pattern/6") >= 0)
            {
              Serial.println("Pattern for Juggle");
              running_pattern = 6;
            }
            else if (header.indexOf("GET /pattern/breathing") >= 0 || header.indexOf("GET /pattern/5") >= 0)
            {
              Serial.println("Pattern for Breathing");
              running_pattern = 5;
            }
            else if (header.indexOf("GET /pattern/confetti") >= 0 || header.indexOf("GET /pattern/4") >= 0)
            {
              Serial.println("Pattern for Confetti");
              running_pattern = 4;
            }
            else if (header.indexOf("GET /pattern/sinelon") >= 0 || header.indexOf("GET /pattern/3") >= 0)
            {
              Serial.println("Pattern for Sinelon");
              running_pattern = 3;
            }
            else if (header.indexOf("GET /pattern/rainbowglitter") >= 0 || header.indexOf("GET /pattern/2") >= 0)
            {
              Serial.println("Pattern for Rainbow glitter");
              running_pattern = 2;
            }
            else if (header.indexOf("GET /pattern/rainbow") >= 0 || header.indexOf("GET /pattern/1") >= 0)
            {
              Serial.println("Pattern for Rainbow");
              running_pattern = 1;
            }
            else if (header.indexOf("GET /pattern/cylon") >= 0 || header.indexOf("GET /pattern/0") >= 0)
            {
              Serial.println("Pattern for cylon");
              running_pattern = 0;
            }
            else if (header.indexOf("GET /quick/green") >= 0)
            {
              Serial.println("Quick Call made to show green");
              notification = true;
              notification_color = CRGB::Lime;
              notification_hold_time = 5000;
            }
            else if (header.indexOf("GET /quick/red") >= 0)
            {
              Serial.println("Quick Call made to show red");
              notification = true;
              notification_color = CRGB::Red;
              notification_hold_time = 20000;
            }
            else if (header.indexOf("GET /quick/reset") >= 0)
            {
              Serial.println("Quick Call made to remove notification");
              notification = false;
            }

            // Display the HTML web page
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<meta charset=\"utf-8\">");
            client.println("<title>LED ping pong control</title>");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            // CSS to style the on/off buttons
            client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
            client.println(".button { background-color: #195B6A; border: none; color: white; padding: 10px 30px;");
            client.println("text-decoration: none; font-size: 18px; margin: 2px; cursor: pointer;}");
            client.println(".button2 {background-color: #77878A;}</style></head>");

            // Web Page Heading
            client.println("<body><h1><del>Death</del> LED ping pong</h1>");

            client.print("<p>Running pattern: ");
            // client.print(running_pattern);
            client.print("</p>");
            client.print(" <select id=\"pattern\">");

            for (byte i = 0; i < sizeof(pattern_names_string) / sizeof(pattern_names_string[0]); i++)
            {
              // Serial.print(i);
              client.print("<option value='");
              client.print(i);
              client.print("' ");

              if (running_pattern == i)
              {
                client.print(" selected");
              }
              client.print(">");
              client.print(pattern_names_string[i]);
              client.println("</option>");
            }
            client.print(" </select>");
            client.println(" </p>");

            // Display current state, and ON/OFF buttons for GPIO 5
            client.println("<p>inbuilt led (lil blue thing) - Currently " + onboard_led_state + "</p>");
            // If the output5State is off, it displays the ON button
            if (onboard_led_state == "off")
            {
              client.println("<p><a href=\"/led/on\"><button class=\"button\">ON</button></a></p>");
            }
            else
            {
              client.println("<p><a href=\"/led/off\"><button class=\"button button2\">OFF</button></a></p>");
            }

            // Display current state, and ON/OFF buttons for GPIO 4
            // This could be used to control something else external, like a relay or other stuff if you map some ports up.
            // client.println("<p>GPIO 4 - State " + output4State + "</p>");
            // // If the output4State is off, it displays the ON button
            // if (output4State == "off")
            // {
            //   client.println("<p><a href=\"/4/on\"><button class=\"button\">ON</button></a></p>");
            // }
            // else
            // {
            //   client.println("<p><a href=\"/4/off\"><button class=\"button button2\">OFF</button></a></p>");
            // }

            // TODO: Add explainer text here:
            // list of the patterns available in url form:
            // for (byte i = 0; i < sizeof(pattern_names_string) / sizeof(pattern_names_string[0]); i++)
            // {pattern_names_string[i] }

            // All done, wrap it up.
            client.println("</body>");
            // Inject JS.
            // Known bug: If you have toggled an led or done an event that changed the url, this fetch call will get confuse this app on
            // what effect should be loaded (indexOf fun)
            String js = R"=====(
            <script>
            document.getElementById("pattern").addEventListener('change', (event) => {
              fetch(`pattern/${event.target.value}`)
            });
            </script>
            )=====";
            client.println(js);
            client.println("</html>");

            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          }
          else
          { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        }
        else if (c != '\r')
        {                   // if you got anything else but a carriage return character,
          currentLine += c; // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
  }

  if (asherhack and notification)
  {
    // Lock the sphere to have no underlying colour to interfere with the notifications
    // fill_solid(leds, NUM_LEDS, CRGB::Black);
    // Found the underlying cause and to fix later:
    // When some pattern effects are applied, they are overwriting the previous "fade amount",
    // So the delta has to start from scratch and never reaches the target colour.
    // If we store how many steps or iterations the function has run, we can reapply it on top 
    // of the running pattern to preserve both the pattern, and fading in colour!
    // IE if a step is 10 and run 5 times, apply a fade here for 50.
  }
  else
  {
    patterns[running_pattern]();
  }

  notifications();

  // Done our processing, now show it.
  FastLED.show();
  FastLED.delay(delay_amount);
  EVERY_N_MILLISECONDS(20) { hue++; } // slowly cycle the "base color" through the rainbow
  EVERY_N_SECONDS(10)
  {
    // Check if we need to save to the eeprom and do it.
    // We wait such a long time before attempting to save to let values "settle" and avoid frequent writes.
    // It also will have a brief pause during this operation
    uint eeprom_led_check;
    EEPROM.get(0, eeprom_led_check);

    if (running_pattern != eeprom_led_check)
    {
      eeprom_led_pattern.anInteger = running_pattern;
      EEPROM.put(0, eeprom_led_pattern);
      EEPROM.commit();
      Serial.println("FWIW: We found the pattern has changed and saved to EEPROM. ");
    }
  }
}