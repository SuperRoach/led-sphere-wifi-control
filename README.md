# LED Sphere project

Although this was designed with the wondeful LED Sphere Project made by ThomasJ152 in mind, in particular this project: https://www.instructables.com/id/LED-Table-Tennis-Ball-Ball/ , it will work with any ESP that has a set of LED Strings connected to it - ping pong balls or otherwise.

Video of the LED Sphere effect:

https://youtu.be/TKvqSvVTzWU

The sphere itself in a mostly complete state, being controlled remotely.
![LED](https://i.imgur.com/rjFCqSOr.jpg)
Screenshot of the web interface.
![Web interface](https://i.imgur.com/S4tIJSx.png)

## Hardware setup
* Using an ESP (This setup is preconfigured to use a ESP-1 or ESP8266), Connect your LED String to the Data pin D8, and power as appropriate (smaller strings can run off the VIN of your ESP, otherwise it will need a seperate powersource)

## Software setup
* Using PlatformIO, or Arduino IDE if you feel comfortable porting it across (be mindful it needs FastLED and WifiManager as includes), upload the code to your device.
* Using a device with Wifi or your phone, connect to the **DeathSphereAP** access point. Fill out the details as needed for it to connect to your Wifi Access Point. This is so you can connect to and control it. 
* If you can, using the monitor will show the IP Address the device has connected to. Otherwise, you can use an IP Scanner on your network ("Network IP Scanner" on Android did the job) to look for an ESP named device.
* Visit the ip address into your browser, and select a colourful pattern to show

## Crash course in using this code

* Git Clone this repo.
* Install PlatformIO. This is an extension for VSCode, and does well in bundling things up to get out of your way, while still giving you nice library dependancy versioning in the .ini files. While it's possible to use VIM for this, it means you have to make your own GCC toolchain for connecting and uploading to the chip, so it's a steep learning curve.
* Open PlatformIO, and the PIO Home screen should be the first thing you see. 
* Click on Open Project (not the arduino one) and point to your repo folder.
* You have a project woohoo! this has saved you a lot of steps in saying what ESP model you have, and the building environment.
* Most of the code is in /src/main.cpp .
* To upload code and have the serial monitor tell you what's going on (think the web dev console), On the far left is a vertical set of icons - click on the alien head probably at the bottom.
* This brings up project tasks. The one you want is "upload and monitor". You can make a shortcut to this if you want, and there is one for upload (ctrl + alt + u) and monitor (ctrl + alt + m) already.
* Read on for some common troubleshooting steps

## API endpoints
You can use the web interface, or alternatively the API to control it directly.

Currently the endpoints are:
* /pattern/cylon
* /pattern/rainbow
* /pattern/rainbowglitter
* /pattern/sinelon
* /pattern/confetti
* /pattern/juggle
* /pattern/bpm
* /pattern/staticblue
* /pattern/staticsun
* /pattern/staticcoolwhite
* /pattern/nothing

## Notification colours
* /quick/red (Fades to bright solid red for 20 seconds)
* /quick/green (Fades to bright solid green for 5 seconds)
* /quick/reset (clears any notification that is visible)

Notifications will briefly boost the brightness to maximum while they are being shown. This is relatively safe as we are using one of the three colours on the LED, so maximum draw is much lower during it.

With any of these endpoint addresses, if you visit them in your webbrowser, it will show that effect straight away. The patterns above can also be accessed by a number too, which is what the select box uses on the webpage (for example /pattern/rainbow is also /pattern/1 ).

## Troubleshooting

* I get errors when I press upload saying it can't find the device.

Plenty of things can go wrong here, but if you are using an ESP8266 and this project, I've narrowed it down a bit for you. When you plug the Sphere in, if your on windows you should hear a new device has plugged in, or a new mount on linux. Once that happens, you have a limited amount of time to talk to it before it might be too busy to respond. I say might, as often you can just do the upload at anytime anyway. It's easiest if you unplug, plug in and then click upload and monitor. In the console window you will get indications on what has gone wrong. Common reasons are "Port is in use" (you need to close the serial monitor from a previous session), or no device found (its busy doing other things and got sick of waiting).

* I want to reset my wifi details I have entered in!

You can awkwardly press the reset buttons on the esp, or you can uncomment this code in main.ccp
```
  //reset saved settings
  // wifiManager.resetSettings();
```
and upload and run it to have a fresh system ready to connect.

* I can't handle the awesome

I know, I know.



This code has been written on the ESP8266. It's a sturdy chip with wifi and bluetooth, and plenty of pins for GPIO/Controlling things. It should be fairly quick to adapt to it's successor the ESP32, with some minor fudging in the notification delay amounts.


## Future plans

It's planned to allow for more API endpoints to do notifications. These will fade in or appear on the sphere when called before disappearing and leaving the existing pattern running. (note: it's almost finished at this point with just more endpoints to add)

I would think these would be along the lines of /quick/green/fade or /quick/red/blink . You could use these in your IoT network to indicate something like a 3D print has failed, or you have a new email, things like that. 

I would also like to allow the user to set a default brightness during the wifi setup (because their powersupply may not handle the user changing it to fullbright), and to toggle the entire running animation to off so it's default state shows no LEDs running.

## Bugs

If you toggle the onboard led, it goes to the address /led/on .
This is fine, but it then means any effects you try to apply will be ignored as it finds that led/on instead. I need to find a better way of determining the address of the page being requested. If you run in to this problem, make sure you are on the base address ( / ) of your sphere of er, lights

